import './App.css';
import RootComponent from './app/navigation/routes';
import {Provider} from 'react-redux';
import store from './app/redux/configurations/store';

const App = () => {
    return (
        <Provider store={store}>
            <RootComponent />
        </Provider>
    );
};

export default App;
