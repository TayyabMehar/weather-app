import { cityConstants } from "../constants";

const initialState = {
    cities: [],
}

function cityReducer(state = initialState, action) {
    switch(action.type) {
        case cityConstants.ADD_CITY_INFO:
            return {
                cities: action.payload,
            }
        case cityConstants.GET_ALL_CITIES:
            return {
                cities: state.cities,
            }
        default:
            return state
    }
}

export default cityReducer;