import {combineReducers} from 'redux';
import city_reducer from './city_reducer';

const reducers = combineReducers({
    city_reducer
});

export default reducers;