import { cityConstants } from '../constants';

export const cityActions = {
    addCity,
    getCities,
};
function addCity (data) {
    return (dispatch, getState) => {
        dispatch({type: cityConstants.ADD_CITY_INFO, payload: data});
    };
}

function getCities () {
    return (dispatch, getState) => {
        dispatch({type: cityConstants.GET_ALL_CITIES});
    };
}