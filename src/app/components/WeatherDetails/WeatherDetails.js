import React, { useState, useEffect } from 'react';
import './style.css';
import DataTable from 'react-data-table-component';
import Moment from 'react-moment';
import moment from 'moment';
import { APIS } from '../../services/weatherService';

const WeatherDetails = (props) => {
    const [history_data, setHistoryData] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        const data = props.history.location.state.data? props.history.location.state.data: null;
        getForecastWeather(data.country.name);
    }, []);

    const columns = [
        {
            name: 'Image',
            left: true,
            cell: row => <div>
                <img className="weather-icon" alt="weather_icon" src={row.condition.icon} />
            </div>
        },
        {
            name: 'Condition',
            sortable: true,
            left: true,
            cell: row => <div>
                {row.condition.text}
            </div>
        },
        {
            name: 'Rain Chances',
            selector: 'chance_of_rain',
            sortable: true,
            left: true,
        },
        {
            name: 'Snow Chances',
            selector: 'chance_of_snow',
            sortable: true,
            left: true,
        },
        {
            name: 'Wind Direction',
            selector: 'wind_dir',
            sortable: true,
            left: true,
        },
        {
            name: 'Humidity',
            selector: 'humidity',
            sortable: true,
            left: true,
        },
        {
            name: 'Time',
            selector: 'time',
            sortable: true,
            left: true,
            cell: row => <div>
                <Moment format="ddd DD-MM-YYYY hh:mm a" date={row.time} />
            </div>
        },
    ]

    const getForecastWeather = (country) => {
        const date = moment().subtract(1, 'days').format('YYYY-MM-DD');
        APIS.getHistory(country, date)
            .then(response => response.json())
            .then(data => {
                console.log('data', data);
                const historyData = (data.forecast && data.forecast.forecastday
                    && data.forecast.forecastday[0] && data.forecast.forecastday[0].hour) ?
                    data.forecast.forecastday[0].hour: null;
                setHistoryData(historyData);
                setLoader(false);
            })
            .catch(err => {
                setLoader(false);
                console.log('some error occurred');
            });
    }

    const goBack = () => {
        const { history } = props;
        history.goBack();
    }

    return (
        <section className="settings">
            <div className="content-wrapper-before "></div>
            <div className="container">
                <div className="content-body">
                    <div className="content-card">
                        <div className="card-header">
                            <h4>Hourly Weather History</h4>
                        </div>
                        <div className="card-body">
                            {loader ?
                                <div className="loader-outer text-center">
                                    <div className="spinner-border" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                </div>:
                                <DataTable
                                    columns={columns}
                                    data={history_data}
                                    pagination
                                    noHeader
                                />
                            }
                            <div className="col-md-auto">
                                <span onClick={goBack} className="btn btn-dark">Go Back</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default WeatherDetails;