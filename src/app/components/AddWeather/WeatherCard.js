import React, { useState, useEffect } from 'react';
import './style.css';
import Moment from 'react-moment';
import { APIS } from '../../services/weatherService';

const WeatherCard = (props) => {
    const [weather_data, setWeatherData] = useState(null);
    const [loader, setLoader] = useState(true);

    useEffect(() => getCurrentWeather(), []);

    const getCurrentWeather = () => {
        const { data } = props;
        APIS.getCurrentWeather(data.city.name)
            .then(response => response.json())
            .then(data => {
                setWeatherData(data);
                setLoader(false);
            })
            .catch(err => {
                setLoader(false);
                console.log('some error occurred');
            });
    }

    const viewDetails =()=> {
        const { data, history } = props;
        history.push('details', {data});
    }
    return (
        <div className="col-md-6 mt-auto mb-auto">
            {(weather_data && !loader) ?
                <div className={"weather-card " + (weather_data.current.condition.text === "Sunny" ? "sunny":
                    weather_data.current.condition.text === "Overcast" ? "haze": 'cold')}>
                    <div className="overlay"></div>
                    <div className="row">
                        <div className="col-md-6">
                            <img className="weather-icon" src={weather_data.current.condition.icon} alt="weather_icon" />
                            <h2 className="text-white font-weight-light position-relative">{weather_data.current.temp_c}
                                <span className="centigrade-sign">&#x2103;</span><span
                                    className="degree"><i className="far fa-circle"></i></span></h2>
                            <h5 className="text-white position-relative">{weather_data.current.wind_mph}
                                <span className="centigrade-sign">mph</span>
                            </h5>
                        </div>
                        <div className="col-md-6">
                            <div className="text-right date-place">
                                <div>
                                    <h4 className="text-white"><Moment format="hh:mm a" date={weather_data.location.localtime} /></h4>
                                    <h5 className="text-white">
                                        <Moment format="ddd DD-MM-YYYY" date={weather_data.location.localtime} />
                                    </h5>
                                </div>
                                <h5 className="text-white">{weather_data.location.name + ', ' + weather_data.location.country}</h5>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-md-6">
                            <span onClick={viewDetails} className="text-white custom-href">View Details</span>
                        </div>
                        <div className="col-md-6 text-md-right">
                            <span className="text-white custom-href" onClick={props.delCard}>Delete</span>
                        </div>
                    </div>
                </div> :
                <div className="loader-outer text-center">
                    {loader ?
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div> :
                        <div className="empty_response" role="status">
                            <span className="y">no data found...!</span>
                        </div>
                    }
                </div>
            }
        </div>
    )
}

export default WeatherCard;