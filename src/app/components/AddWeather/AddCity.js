import React, { Component } from 'react';
import { connect } from 'react-redux';
import './select.css';
import './style.css';
import SelectSearch from 'react-select-search';
import csc from 'country-state-city';
import { cityActions } from '../../redux/actions';
import WeatherCard from "./WeatherCard";

class addCity extends Component {
    constructor(props){
        super(props);
        this.state = {
            countries: [],
            cities: [],
            selected: {
                country: null,
                city: null,
            },
            cardsData: [],
            showCards: true
        }
    }

    componentDidMount () {
        const { dispatch } = this.props;
        dispatch(cityActions.getCities());
        this.setCountries();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.state.city_reducer.cities.length !== prevState.cardsData.length) {
            return {
                cardsData: nextProps.state.city_reducer.cities
            }
        }
        // Return null to indicate no change to state.
        return null;
    }

    setCountries = () => {
        let countries = [];
        const allCountries = csc.getAllCountries();
        allCountries.map(item => {
            countries.push({
                value: item.id,
                name: item.name,
            })
        })
        this.setState({countries});
    }

    getStatesAndSetCities = (countryId) => {
        let selected = { ...this.state.selected };
        const { countries } = this.state;
        const filterCountry = countries.filter(obj => obj.value === countryId);
        selected.country = filterCountry.length !== 0 ? filterCountry[0]: null;
        selected.city = null;
        let cities = [];
        const allStates = csc.getStatesOfCountry(countryId);
        allStates.map(item => {
            const allCities = csc.getCitiesOfState(item.id);
            let newCities = [];
            allCities.map(city => {
                newCities.push({
                    value: city.id,
                    name: city.name,
                })
            })
            cities = [...cities, ...newCities];
        })
        this.setState({cities, selected});
    }

    selectCity = (cityId) => {
        let { selected, cities } = this.state;
        const filterCity = cities.filter(obj => obj.value === cityId);
        selected.city = filterCity.length !== 0? filterCity[0]: null;
    }

    addCity = () => {
        const { dispatch } = this.props;
        let { selected, cardsData } = this.state;
        cardsData.push(selected);
        this.setState({selected, cardsData});
        dispatch(cityActions.addCity(cardsData));
    }

    delCard = (index) => {
        let that = this;
        this.setState({showCards: false}, () => {
            const { dispatch } = this.props;
            const array = [...this.state.cardsData]; // make a separate copy of the array
            array.splice(index, 1);
            dispatch(cityActions.addCity(array));
            that.setState({showCards: true});
        });
    }

    render() {
        const { countries, cities, selected, cardsData, showCards } = this.state;
        const { history } = this.props;
        return (
            <section className="settings">
                <div className="content-wrapper-before "></div>
                <div className="container">
                    <div className="content-body">
                        <div className="content-card">
                            <div className="card-header">
                                <h4>Weather Scenes</h4>
                            </div>
                            <div className="card-body">
                                <div className="row mb-3">
                                    <div className="col-md">
                                        <div className="form-group row">
                                            <label htmlFor="firstName" className="col-sm-4 col-form-label">Select
                                                Country</label>
                                            <div className="col-sm-8">
                                                <SelectSearch search options={countries} value={selected.country ? selected.country.value: null} onChange={this.getStatesAndSetCities} name="country" placeholder="Choose your country" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md">
                                        <div className="form-group row">
                                            <label htmlFor="firstName" className="col-sm-4 col-form-label">Select
                                                City</label>
                                            <div className="col-sm-8">
                                                <SelectSearch search options={cities} value={selected.city ? selected.city.value: null} onChange={this.selectCity} name="city" placeholder="Choose your city" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-auto">
                                        <span onClick={this.addCity} className="btn btn-orange">Add</span>
                                    </div>
                                </div>

                                <div className="row">
                                    {showCards && cardsData.map((item, i) => {
                                        return <WeatherCard
                                            key={'' + item.country + item.city + i}
                                            data={item}
                                            delCard={()=>this.delCard(i)}
                                            history={history}
                                        />
                                    })}
                                </div>
                                {cardsData && cardsData.length> 0 &&
                                <div className="text-center data-note">
                                    <p>Instead of use local storage directly I have integrated
                                        <span className="redux-persist"> REDUX-PERSIST</span>, data will be saved when you press Add button above</p>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = state => {
    return {
        state
    }
}
export default connect(mapStateToProps)(addCity)