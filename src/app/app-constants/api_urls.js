import app_constants from './constants';
export default {
    current_weather: app_constants.base_url + 'current.json?key=' + app_constants.api_key + '&q=',
    history_weather: app_constants.base_url + 'history.json?key=' + app_constants.api_key + '&q=',
}