import api_urls from "../app-constants/api_urls";

export const APIS = {
    getCurrentWeather(city) {
        return fetch(api_urls.current_weather + city);
    },
    getHistory(country, date) {
        return fetch(api_urls.history_weather + country + '&dt=' + date);
    }
}