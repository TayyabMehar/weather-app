import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import addCity from '../components/AddWeather/AddCity';
import WeatherDetails from "../components/WeatherDetails/WeatherDetails";
import history from './history';

export default () => {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={addCity} />
                <Route exact path="/details" component={WeatherDetails} />
            </Switch>
        </Router>
    )
}
